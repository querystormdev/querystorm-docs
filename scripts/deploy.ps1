$privateKeyPath = $env:SFTP_PRIVATE_KEY
$localDirectory = Join-Path -Path (get-location) -ChildPath "site"
& "C:\Program Files (x86)\WinSCP\WinSCP.com" `
  /command `
    "open sftp://querystorm@ftp.querystorm.com/ -privatekey=$privateKeyPath -hostkey=*"`
    "synchronize remote -delete -criteria=size $localDirectory ./public_html/docs2" `
    "exit"

if ($LASTEXITCODE -ne 0) {
  Write-Error "WinSCP script execution failed"
  exit $LASTEXITCODE
}   