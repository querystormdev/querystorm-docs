
# Debugging

QueryStorm apps can be debugged using QueryStorm's built-in .NET debugger.

![Debugger (docked)](https://querystorm.com/docs2/images/debugger_docked.png)

Since the debugger runs inside the Excel process, apps that are being debugged are  executed inside a separate runner process. This is done only for the app that's being debugged, other apps run inside the Excel process as usual.

When debugging workbook apps with large amounts of data, running the app in a separate process slows down reading and writing operations. To avoid that, the debugger can be launched as a separate application that runs in its own process, allowing the debugee (the QueryStorm app) to run inside the Excel process.

![Debugger external start](https://querystorm.com/docs2/images/debugger_external_start.png)

![Debugger external IDE](https://querystorm.com/docs2/images/debugger_external_ide.png)

The debugger behaves slightly differently when docked compared to when running as an external application. Most notably, when docked, it does not block the Excel user interface when it hits a breakpoint (otherwise it would block itself as well since it's running inside Excel). When running as a separate app, it does block Excel while stopped at a breakpoint.

In general, using the external QueryStorm debugger is advisable when working with workbook apps with large amounts of data. For other scenarios, both debuggers should work fine.

The debugger uses the same shortcuts as Visual Studio. These can be customized by the user:

![Debugger shortcuts](https://querystorm.com/docs2/images/debugger_shortcuts.png)

## Alternative debugging options

Aside from the built-in debugger, but there are two static methods that can also help with debugging: `Log()` and `Debug()`.

![YOUTUBE](zqPGuJoD5DM)

### The `Log()` method

The simplest way to debug issues is to use the `Log(object obj)` method to print values to the messages pane.

The `Log()` method is contained in the `QueryStorm.Core.DebugHelpers` class. All class files that QueryStorm generates have a static *using* directive for that class, so you can use the `Log` method anywhere in your code, without qualifying it with the namespace or the class name.  

It's important to note that QueryStorm has two log viewers. One is part of the IDE, and the other is part of the Runtime (launched separately from the ribbon). The output of the `Log()` method will be visible in both places, so Runtime users will be able to see these messages.

### Attaching an external debugger

The `Log()` method is useful, but quite often a proper debugger is needed to track down tricky bugs. QueryStorm compiles code in a debugger-friendly way, so it's fairly easy to debug your code with an external debugger.

To launch a debugger at a particular location in the source code, use the `Debug()` method. The `Debug()` method is also available anywhere in the code without prefixing it with the namespace or the class name, due to the `using` directive that's part of all code files scaffolded by QueryStorm.

If the local machine has Visual Studio installed, the `Debug()` method will launch Visual Studio, attach it to the process and stop the debugger at the current line. If a debugger is already attached, it will simply stop at the line with the `Debug()` call.
