# QueryStorm Apps

QueryStorm lets users build apps that add functionality to **a particular workbook** or to **Excel itself**.

An app is created by building (compiling) a [project](../projects/overview.md). A project consist of source code and resource files, while an app consist of the compilation outputs of a project, namely one or more dlls and a manifest file.

An app can be defined inside of a particular workbook (workbook app) or globally (extension app). Aside from their locations and purposes, there is little technical difference between the two.

## Workbook apps

Workbook apps add functionality to a particular workbook. They are particularly useful for prototyping and building ad-hoc applications which can be built quickly, since the starting point is not a blank form or page, but the full functionality of Excel.

![Automation example](../images/automation_db_example.gif)

Workbook apps:

- can automate the workbook, as well as define custom functions, ribbons and context menus.
- start when the workbook is opened and stop when the workbook closes.
- have strongly typed access to the tables inside the workbook.
- are stored and shared inside the workbook (though keeping the source code in the workbook is optional).

## Extension apps

An extension app is a **package of extra functionality for Excel** that is available globally (in any workbook). Typically, an extension app will define a set of custom functions and commands related to a particular topic.

For example, the `Windy.Text` package which can be downloaded from the official QueryStorm feed defines a set of custom Excel functions for text manipulation:

![Extension app windy.text install](../images/ext_apps_install_text.png)

![Extension app example](../images/apps_ext_example.png)

Extension apps are stored at the machine level and are available inside any workbook. They are started when Excel starts and stopped just before Excel closes. Since they are not part of a workbook, they are shared independently.

The QueryStorm IDE provides a mechanism for [publishing extension apps](../projects/publishing.md) to the cloud or a network share, while the runtime provides an Extension manager that allows [downloading and installing](../runtime/installing_packages.md) extension apps from these repositories.

## Execution requirements

QueryStorm apps are executed by the [QueryStorm Runtime](../runtime/overview.md) which must be installed on end user machines. The QueryStorm runtime can be installed on Excel 2007 and higher.
