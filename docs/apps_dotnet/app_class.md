# The `App` class

Each QueryStorm project has an `App` class that's defined in the `App.cs` (or `App.vb`) file. This is the entry point of the app.

## The constructor

The constructor of the app class receives an `appHost` argument which gives the app access to certain services that the host environment (i.e. QueryStorm Runtime) provides.

![app host members](../images/apphost.png)

The services that the Runtime provides are:

- An `IExcelAccessor` which allows accessing the root Excel `Application` object.
- An `IWorkbookAccessor` which allows accessing the current workbook. For workbook apps, this will be workbook that contains the app. For extension apps this will be the active workbook.
- An `IShortcutService` which allows registering custom shortcuts.
- An `IQueryStormLogger` which allows logging to the log window (runtime) and the messages pane (IDE).

## The `Start()` method

The `Start` method is the place to perform any startup logic for your app. It is called when the app is started. For a workbook app, this will be when the workbook is opened. For an extension app, this will be when Excel starts.

For example, if the app needs to react to a keyboard shortcut, it should put the registration logic in the `Start` method:

```csharp
public class App : ApplicationModule
{
    public App(IAppHost appHost)
        : base(appHost)
    {
    }

    public override void Start()
    {
        // first, allow the base class to start up
        base.Start();
        
        // register a shortcut (Ctrl+Alt+F1) to pop up a message box
        var shortcutService = Container.Resolve<IShortcutService>();
        var shortcut = new ShortcutBinding(
            Key.F1,
            ModifierKeys.Alt | ModifierKeys.Control,
            () => MessageBox.Show("Hello"));
        shortcutService.RegisterShortcut(shortcut);
    }
}
```

When overriding the `Start` method, it is important to call the base class's `Start()` method first. The base class's Start method does the following:

- [registers dependencies](dependency_injection.md) with the IOC container
- instantiates and starts [components](components.md)
- scans the assembly for a [ribbon](ribbons.md) and [context menu commands](context_menus.md) and registers the ones it finds

## The `Stop()` method

The stop method is invoked when the app is shutting down. For workbook apps this will be when the workbook is closing. For extension apps this will be when Excel is closing.

Apps should override this method if they have any cleanup to do (e.g. closing connections, disposing of resources).

```csharp
public override void Stop()
{
    // cleanup code goes here

    // do not forget to let the base class clean up
    base.Stop();
}
```

When overriding the `Stop()` method, it is important to call the base class's `Stop()` method in the end, since the base class uses it to perform cleanup as well (e.g. unregister context menus and ribbons, dispose of the container).
