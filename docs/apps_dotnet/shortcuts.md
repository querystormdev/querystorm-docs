# Keyboard shortcuts

QueryStorm apps can hook up their own keyboard shortcuts, making it easy for users to invoke actions on the app. These shortcuts override built-in Excel shortcuts.

## Defining a shortcut

To define a shortcut, the app must get a reference to the shortcut service and register the shortcut. Usually, the `Start()` method is the appropriate place to register the shortcut.

```csharp
public class App : ApplicationModule
{
    public App(IAppHost appHost)
        : base(appHost)
    {
    }
    
    public override void Start()
    {
        base.Start();
        
        IShortcutService shortcutService = Container.Resolve<IShortcutService>();
        var binding = new ShortcutBinding(
            Key.Y,
            ModifierKeys.Control | ModifierKeys.Alt,
            () => MessageBox.Show("123"));
        shortcutService.RegisterShortcut(binding);
    }
}

```

## Limiting shortcut scope

Shortcuts can be limited to a particular scope. The user is free to define the scope of shortcuts however they like using the `isInScope` parameter which is of type `Func<bool>`.

For example, here's how to make a shortcut available only when the current selection in Excel overlaps a table:

```csharp
var excelAccessor = Container.Resolve<IExcelAccessor>();

var binding = new ShortcutBinding(
    Key.Y,
    ModifierKeys.Control | ModifierKeys.Alt,
    () => MessageBox.Show("123"),
    // shortcut is available only when the selection touches an Excel table
    () => excelAccessor.Application.Selection is Range r && r.ListObject != null);
```

If the shortcut is not in scope, it will be ignored and the keystrokes will be passed through to Excel.

## Workbook-scoped shortcuts

Shortcuts can be quite useful in workbook apps. Instead of navigating to a sheet with a particular button, the user can invoke an action from anywhere in the workbook using a keyboard shortcut.

In the case of workbook apps, **all shortcuts are automatically limited to the scope the owning workbook**. When the workbook is not the active workbook in Excel, its shortcuts will not be triggered.
