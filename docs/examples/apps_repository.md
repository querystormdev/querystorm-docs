# Apps source code repository

To make it easier for users to learn how to build apps, we publish the source code of all of our official QueryStorm apps on GitLab.

Each app is published into its own repository. For example, you can find the source code of the [Windy.Text](https://gitlab.com/querystormdev/windy.text) and [Windy.Searchlight](https://gitlab.com/querystormdev/windy.searchlight) apps in their respective repositories.

If you're interested in building your own apps in QueryStorm, consider browsing through these repositories for ideas on what you can build as well as answers to any technical questions you might have.

## The catalog repository

In addition to the individual app repositories, there is also a root repository called [QueryStorm Apps](https://gitlab.com/querystormdev/querystorm.apps), which contains all of the app repositories as submodules. You can clone this repository to get the source code for all of the individual apps at once.

![QueryStorm Apps repo](../images/gitlab_qsapps_repo.png)

The source code of apps included in the [QueryStorm Apps](https://gitlab.com/querystormdev/querystorm.apps) repository is periodically compiled and the resulting packages are published to the "Official QueryStorm Extensions" feed that is available to all QueryStorm users out-of-the-box.

![Public extensions](../images/extensions_browse.png)

## Contributing your own apps

If you'd like to include a useful general-purpose app of your own to the official feed, we'd welcome your contribution. Note that this would involve publishing the source code of your app into a public gitlab repository, which means your app would be open source (though not necessarily free).

To request adding your own app to the official feed, please do the following:

1. Create a Git repository in the folder that contains your app.
2. Create a GitLab repository that will host your app's source code online.
3. Push the source code of your app from your local repository into the the GitLab repository.
4. Clone the [QueryStorm Apps](https://gitlab.com/querystormdev/querystorm.apps) repository locally and, in a new brach, add the GitLab repository (from step 2) as a submodule.
5. Push your branch and create a pull request to merge your changes into the master branch of the QueryStorm Apps repository.

Once the merge request is accepted, the code will be automatically compiled and the resulting app will be published to the "QueryStorm Official Extensions" feed.

The license under which you publish the source code is up to you, however, it must be an open source license. If not specified, the permissive [MIT license](https://opensource.org/licenses/MIT) is assumed. All intellectual property rights to your code remain your own.

If you need help with submitting your app to the repo, please feel free to [contact us](mailto:support@querystorm.com) directly.