# Feedback

This documentation is hosted on a public GitLab [repository](https://gitlab.com/querystormdev/querystorm-docs). We encourage all users to use this repository as a public forum for discussing issues related to both QueryStorm as well as this documentation.

Please feel free to [start an issue](https://gitlab.com/querystormdev/querystorm-docs/-/issues) whenever you'd like to:

- report an issue
- request a feature
- ask a question
- provide feedback
- start a general discussion
