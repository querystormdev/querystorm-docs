# The data context

The data context represents the outside world to your scripts and apps. It exposes tables, variables, and events.

In scripts, user code gets direct accesses to the tables and variables exposed by the data context.

![object explorer](../images/datacontext_objectexplorer.png)

Apps, on the other hand, usually interact with the data context via bindings on component classes. Component properties can bind to tables and variables, while methods can handle events coming from the data context.

![data binding](../images/bindings_annotated.png)

Projects specify the data contexts they use in the `DataContextTypes` section of the `project.config` file.

![Project data contexts](../images/project_data_contexts.png)

In workbook apps, the default data context is a built-in type called `WorkbookDataContext` that exposes data and events from the workbook.

Extension projects start off without a data context, but one or more data contexts can be added manually to provide convenient access to external data sources.

QueryStorm allows adding, removing and customizing data contexts in both workbook and extension projects. When more than one data context is defined, a `MergedDataContext` instance will be used to combine them into a single data context. Scripts and apps will then be able to use the tables and variables from all of the defined contexts.
