# Custom data contexts

QueryStorm allows defining custom data contexts that allow scripts and apps to work with various data sources.

A data context is a class that implements the `IDataContext` interface. However, implementing an entirely new kind of data context is likely to involve considerable effort (and is out of scope of this documentation) so inheriting from the existing `WorkbookDataContext` and `CSVDataContext` types is much more common.

## DataContext definition scripts

Importantly, a data context class is not defined in a regular class file, but in a special kind of C# script - a **data context definition script**. These scripts can be created from the context menu of the project.

![Add workbook data context command](../images/datacontext_add_menu.png)

The result of running this kind of script is **not an output value, but a dll** that contains the compiled class contained in the script. This dll is automatically placed inside the `lib` folder.

![Data context generate dll](../images/datacontext_generate_dll.gif)

The code in the script cannot reference any of the other types in the containing project. Since the script does not depend on the rest of the project, the data context can be customized independently of the rest of the project.

## Registering the data context type

Each time a data context script is executed, it updates the `DataContextTypes` section of the `project.config` file to include the type in the newly generated dll.

![Project data contexts](../images/project_datacontext_custom.png)

Scripts and apps use the `DataContextTypes` section of the config file to look up which data context(s) to instantiate and use.

## Strongly typed accessors

Each time a data context changes (the context definition script is executed or the schema of the context changes), the project will automatically generate an additional `context_types.{xxxxxxxx}.dll` in the `lib` folder containing wrapper classes that give strongly typed access to the data context. These types are important for components and C# scrips, but are not used in SQL scripts. Click [here](typed_accessors.md) for more information on typed accessors.
