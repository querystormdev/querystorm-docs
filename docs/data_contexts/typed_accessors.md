# Strongly-typed data accessors

Data contexts offer access to data from various sources, but this access is not strongly typed. This is not a problem for SQL scripts, but .NET code and C# scripts can benefit from strongly typed access to the data context.

To add strongly typed access to the data context, projects in QueryStorm automatically generate a dll which contains wrapper types for all tables inside the data context. The file is saved into the `lib` folder and uses the following naming convention: `context_types.{hash}.dll`.

![Context types dll](../images/project_context_types.png)

Once this dll is generated, user code can make use of the strongly typed wrappers for the tables in the data context.

![Data context accessors](../images/datacontext_accessors.png)

## Updating the strongly-typed accessors

This dll file is automatically recreated whenever the data context of the project changes. For workbook data contexts this will happen when a table or a named range is added/removed or when an existing table's structure is changed.

![Updating workbook data context accessors](../images/datacontext_updating_accessors.gif)

The context types dll is also recreated whenever the `project.config` file is saved, so data context scripts also trigger it when they register their output types in the `DataContextTypes` list.

Regardless of how many data contexts a project defines, there will be only one dll that will define strong types for all of their tables.

![Accessors dll](../images/datacontext_accessors_dll.png)