# How it works

Defining Excel functions and event handlers in QueryStorm is normally done through C# or VB.NET code. However, this code does not need to be written by hand. Instead, it can be generated automatically based on the preprocessor code in a SQL script.

When a SQL script (that has a preprocessor declaration) is saved, a code-behind file is automatically generated and nested under the script.

![SQL script code-behind](../images/sql_script_codebehind.png)

The generated class will contain a method that connects to the database and executes the SQL command contained inside the script. Specifics of this class and method depend on the kind of preprocessor declaration that was used.

Once the code files are generated, all that's needed to run the app is to build the project.

![Build project](../images/commands_build.png)

When the app is built, the compiled files of the app are placed inside the `bin` folder of the project. The runtime then detects the new app and loads it.
