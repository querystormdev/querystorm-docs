# Managing extensions

Extension app are created and published using the QueryStorm IDE by package creators and are installed and used through the QueryStorm Runtime by end-users. An extension app contains a related set of functionalities  (custom functions, UI elements, keyboard shortcuts etc...) that extend Excel with additional capabilities, for a particular purpose.

A typical scenario for QueryStorm extension apps is as follows:

1. A developer uses the QueryStorm IDE to write a set of Excel functions for a particular purpose
2. Once they've prepared the functions, they publish the package that contains them to a feed
3. End-users install the package using the Extensions manager in the QueryStorm Runtime
4. End-users use the new functions in their Excel workbooks

## Installing extension apps

Apps are installed and managed from the *Extensions Manager* dialog as shown below.

![Installing packages](../images/installing_packages.gif)

## Managing feeds

Creators publish their apps to a feed (technically, a NuGet repository). This is typically a feed that's owned by the package creator. Out of the box, only the *QueryStorm Official Feed* in included, which contains extension apps created by the QueryStorm team.

> The other included feed, nuget.org, is used by the IDE for regular NuGet packages and not for extension packages.

To be able to browse packages that another creator has prepared, you must add their feed into your list of feeds. The creator is responsible for providing you with the URL to their feed.

Once you have the URL, you can add it to your list of feeds. The list of feeds is edited in the **Extensions Manager** dialog as shown below.

![Edit package sources](../images/edit_package_sources.png)

1. Area for managing sources (feeds)
2. Button for editing the feed
3. Feed URL or path
4. Feed content type (Packages, Extensions or Both)

> Both creators and consumers use the above dialog to edit their package feeds.

It is important to set the `FeedType` setting to `QueryStorm Extensions` or `Packages And Extensions`, otherwise the feed will not be used when searching for extension apps.

