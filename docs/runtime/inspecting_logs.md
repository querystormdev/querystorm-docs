# Runtime logs

The logs dialog is the simplest way to detect problems in apps. The runtime logs can be displayed by clicking the "Show logs" button:

![Runtime logs button](../images/runtime_logs.png)

This will open the log viewer dialog:

![Runtime logs dialog](../images/runtime_logs_dialog.png)

The dialog displays logs from the runtime itself, but also any log messages sent from running apps.

Hovering items in the log shows a tooltip with the full message. This is especially useful for error messages which tend to be longer than one line.
