# Configuring apps

Apps can optionally have configuration settings that allow the user to configure them. For example, settings for an app might include API keys, credentials and user preferences such as keyboard shortcuts.

To access app settings, click the "Configure extensions" dialog.

![Runtime configure apps button](../images/runtime_configure_apps.png)

This will open a dialog that allows configuring apps. Selecting the app (on the left) will display its settings UI.

![Runtime configure apps button](../images/runtime_configure_apps_dialog.png)