# Trusted workbooks

When opening a workbook, the runtime checks if it contains a workbook app. An app can potentially contain harmful code, so executing it outright without prompting the user would be a security hazard, especially since the user might not even be aware that the workbook contained any code.

For this reason, before running a previously un-encountered workbook app, the runtime will prompt the user to verify that they trust the app.

![Trust prompt](../images/trust_prompt.png)

Once a workbook app is trusted, the user will not be prompted again for that same workbook app.

If the user indicates they do not trust the app, the workbook will be opened as a standard Excel workbook, but the app inside the workbook will be inactive. The user will be prompted again the next time they open the workbook.

> Based on user feedback, we might revise the user experience for this feature. In particular, we're considering offering two more options: "trust once" and "refuse permanently" (though the latter seems to make more sense). We would also include a button in the ribbon to allow trusting/untrusting an app at a later point.

## How it works

A workbook app consists of one or more dlls. When a workbook app is compiled, the IDE creates a manifest file that describes the contents of the app. Among other things, the manifest contains a list of dlls (managed and native) that the app consists of as well as a hash of each dll file's bytes.

When the runtime attempts to load a workbook app, it creates a hash of the manifest file and uses this hash to check if the app is trusted. If it does not have a record of this hash (i.e. the user has not trusted this document yet), it will prompt the user to confirm they trust the document.

If the user confirms that they trust the document, a small file is created in the `%appdata%\QueryStorm2\TrustedApps` folder. The name of this file will be the hash of the manifest, and the contents will be information about the workbook app (original location, manifest contents). When this app is opened in the future, the runtime will find this file and based on its existence, will conclude that the workbook app is allowed to run.

## What if the workbook changes?

Trust covers only the dlls and the manifest, so the user can freely change data inside the workbook without affecting trust. Only if the dlls or the manifest file change will the user be prompted again.

This offers a good level of security, even for shared workbooks, because:

- If any of the dlls is tampered with, the hash of that dll in the manifest file will be different and the runtime will refuse the load the dll.
- If the manifest file itself is tampered with, its hash will be different so it will be treated as a new (not-yet-trusted) app.

## Avoiding the check for locally-built apps

When building workbook apps locally, you are the author of the app so it makes no sense for the QueryStorm runtime to ask you if you trust the app. To avoid the prompt, the IDE will preemptively register the app as a trusted app before signalling to the runtime to load it.

To make it easier to distinguish workbooks that are automatically trusted by the IDE, the trust record is stored in a subfolder: `%appdata%\QueryStorm2\TrustedApps\Local`. This folder will contain many records as the user builds various workbook apps, but can be freely deleted at any time.

## No check for extension apps

Technically, extension apps could be checked the same way during load time, but since they are downloaded explicitly by the user, this would be redundant. With workbooks, the user might not be aware if there's an app inside, so they must be explicitly prompted before allowing any code inside of a workbook to run.
