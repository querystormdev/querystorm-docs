# Project system

Code in QueryStorm is organized into projects. Projects contain user code, scripts, and configuration files, and are fairly similar to projects in Visual Studio.

![Project system screenshot](../images/project_system_screenshot.png)

When a project is compiled it produces one or more dlls and a manifest file which together form an **app**.

## Two kinds of projects

There are two kinds of projects in QueryStorm: Workbook projects and Extension projects.

**Workbook projects** are defined inside a particular workbook and serve to automate the workbook. They can contain automation code as well as custom functions and menus that are specific to the containing workbook. Both the code (the project) and the output files (the app) are stored inside the workbook itself.

**Extension projects** are defined at the machine-level, and serve to define functions and commands that should be available in all workbooks. Building (compiling) an extension project produces an extension app. Extension apps can be shared with other users by publishing them to the cloud or a shared network folder. They can then be downloaded by end-users via the Extensions Manager that's part of the QueryStorm runtime.

## Supported languages

QueryStorm supports writing code in **C#**, **VB.NET**, and **SQL**.

The primary way to build apps is using .NET (C# or VB.NET) which offers maximum power and flexibility.

However, many users will not be familiar with .NET languages, but will be familiar with SQL. For this reason, QueryStorm contains a simple SQL preprocessor that gives users an easy way to automate queries and define custom Excel functions with SQL scripts.
