# Managing workbook code

Workbook projects are stored inside of the workbook, which has implications on versioning of the code as well as intellectual property.

## Importing and exporting code

Workbook projects can be exported to a folder on the local machine and later imported into another workbook (or the same workbook).

This makes it possible to manage the code separately from the data in the workbook and enables the following scenarios:

- updating an existing workbook with a new version of the code
- reusing the same code in multiple workbooks
- versioning workbook code using Git (or another source control system)
- protecting intellectual property by keeping only the output dlls in the workbook while keeping the source code out of it.

The following animation illustrates the process of exporting a project and importing it into a new workbook:

![Import/export](../images/import_export_project_pb.gif)

## Protecting intellectual property

Keeping the source code inside the workbook is not necessary. The runtime only needs the build outputs inside the `bin` folder in order to run the workbook app. To protect your intellectual property, export your code to a folder on your local file system, and then remove everything except the `bin` folder from the workbook project before sharing the workbook with others.

It's important to note that the protection this provides is not absolute as the dlls can be extracted from the workbook and decompiled using one of the many available .NET decompilation tools. QueryStorm currently does not offer any tools for obfuscating compiled dlls. In case protection against decompilation is needed, the sensitive parts of the code should be compiled and obfuscated with an external IDE and obfuscator, and the resulting dll can then be referenced from the workbook project.
