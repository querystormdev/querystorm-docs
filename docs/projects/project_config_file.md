# Project configuration

Projects are configured by editing the `project.config` json file. This file can be edited manually, but individual sections of the file can also be (and usually are) edited through various dialogs.

## Basic info

The `project.config` file has several settings that describe the basic information about the project: `Name`, `Version`, `Tags`, `Authors`, `IconUrl`, `Description`, and `ProjectUrl`.

This information is usually specified when publishing extension projects, as it is used to give potential users information about the package and its contents.

![Project basic info](../images/project_config_basic_info.png)

> The `Summary` setting is not shown in the UI as it is generated automatically based on the detected contents of the package.

When a package is published, potential users will see the information about the project in the "Extensions manager" dialog.

![Published package](../images/package_manager_querypkg.png)

> The summary information will be visible in the tooltip.

## References

A project can reference NuGet packages, local managed dlls and local native dlls.

![Project references](../images/project_config_references.png)

### NuGet references

NuGet packages can be installed and uninstalled using the package manager dialog. Installing a package will add the entry to the `Dependencies` section, though this section can also be edited manually. When editing this section manually, the "Restore packages" command should be executed in order to ensure the packages are installed.

![Restore packages](../images/commands_restore_packages.png)

### Managed references

Managed references are listed in the `References` section. Dll files that are in the GAC or are part of the QueryStorm IDE can be specified by name (without the folder path). All other dlls must be specified by their full file system path. When referencing a local file by path, the file is copied at compile-time (not at reference time), so it's possible to work on a dll separately in Visual Studio and the QueryStorm project will always reference the fresh version.

### Native references

Native references are listed in the `NativeReferences` section. Dll files that come with the QueryStorm IDE (such as the SQLite interop dlls) can be referenced by relative path. All other dlls should be specified by absolute path. On build, the dlls are copied to the `bin\x86` or `bin\x64` folders, based on the detected architecture of the dlls. The runtime will load any native dlls that it detects first, in order to allow managed code to P/Invoke into them.

## Connection strings

Connection strings are specified in the `ConnectionStrings` section of the `project.config` file and are separated by type (`SQLServer`, `SQLite`, `MySQL`, `Postgres`).

![Connection strings](../images/project_config_connection_strings.png)

Connection strings can be added to a project when configuring the connection for a SQL script.

![Connection strings](../images/connect_dialog.png)

Instead of creating a new connection string, though, scripts can also choose to use an existing connection string. Sharing connection strings this way makes it easy to switch multiple scripts from one database to another (e.g. development to production).

### Templated connection strings

In order to protect credentials and to ensure each user uses their own credentials (and level of access), connection strings can be templated, such that the username and password are kept out of the connection string. To template a connection string, use `{username:some_id}` and `{password}` placeholders instead of the actual database credentials, for example:

```connstr
Data Source=myServer;Initial Catalog=thingieq_AdventureWorks2014;User ID={username:for_my_adv_works};Password={password};MultipleActiveResultSets=True
```

Before connecting (for the first time), the user will be prompted to enter their db credentials. If the connection is successful, the credentials will be saved under the specified identifier (e.g. `for_my_adv_works` in the example above). More information about securing connection string credentials can be found [here](../apps_sql/securing_credentials.md).

## Data context types

The `DataContextTypes` section of the `project.config` file specifies a list of types that will be used as the data context for the project. The IDE uses this list to construct a data context instance that will be used by scripts to get access to data (e.g. workbook tables and variables). This list is also used by the app itself (at runtime), to create a data context instance that components can data-bind to. If multiple types are specified, a merged data context instance will be created and used.

![Data context types](../images/project_config_datacontexttypes.png)

## Language selection

QueryStorm projects support source code written in C# and VB.NET. While C# is more popular with developers, VB.NET is more familiar to people with VBA experience. The two languages have somewhat different syntax but are practically identical in capabilities. Source code for both languages is compiled using Microsoft's Roslyn compiler.

You can choose the language for your project when you are creating it.

![New project dialog](../images/new_project_dialog.png)

This setting is saved in the `project.config` file and can be changed manually at a later point if needed:

```json
"Language": "CSharp"
```

The allowed values are "CSharp" and "VisualBasic". Alternatively, you can use the number 1 (without quotes) for C# and 2 for VB.NET.

This setting affects the compilation of the project, as well as the class templates that are used when adding new files to projects.
