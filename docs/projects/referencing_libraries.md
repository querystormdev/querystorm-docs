# Referencing libraries

Projects can reference NuGet packages as well as local dlls (managed or native).

## NuGet packages

Projects can reference NuGet packages from the official `nuget.org` repository or any other repository that the users configures.

![Add NuGet package menu](../images/project_manage_packages_command.png)

Clicking on the "Manage packages" commands opens up the package manager dialog:

![Add NuGet package menu](../images/project_manage_packages_dialog.png)

Installing a package downloads the package to the local file system and adds a record of the dependency into the `project.config` file:

![Project dependencies](../images/project_config_dependencies.png)

On each build, the project will locate the files that belong to the package, reference them and copy them into the `bin` folder.

![Referenced NuGet dll](../images/nuget_referenced_dll.png)

Out of the box, `nuget.org` is the only defined source of NuGet packages, but the list of package sources can be edited in the "Manage sources" tab of the package manager dialog.

![Manage NuGet package sources](../images/nuget_manage_package_sources.png)

When configuring a package source, it is important to choose the appropriate "Feed type". This setting determines whether the source will be used when searching for nuget packages, QueryStorm extensions or both.

## Managed local dlls

Projects can reference dlls from the local file system:

![Add local dll](../images/project_add_dll.png)

Adding a reference to a dll adds a record to the `References` section of the `project.config` file:

![Local dll reference](../images/project_local_dll_reference.png)

The file is referenced by its full path. On build, the dll file is copied into the `bin` folder. Since the file is copied on build, it can be updated in the meantime (e.g. in Visual Studio) and the app will use the file as it is at build time, rather than as it was at reference time.

## Native local dlls

Native dll references can be added manually to the `NativeReferences` section in the `project.config` file. On build, the files are copied to the `bin\x86` and `bin\x64` folders based on the detected architecture of the dlls.

![Adding native dll reference](../images/project_native_references.png)

Native dlls inside NuGet packages are automatically detected and added to the `x86` and `x64` folders as well.

When loading apps, the runtime first loads all native dlls it finds before starting to load the managed dlls. This enables managed code to call the functions inside the native dlls using P/Invoke.
