# Files and folders

The project system supports several kinds of files, most of which are visible in the "Add new file" context menu:

![Add files context menu](../images/project_files.png)

- **Application class**: scaffolds a class that is the entry point of the application and serves to initialize services, the data context, components, and and commands. There is a maximum of one `App` class per project.
- **Class**: these contain the business logic of your app (services, tools, helper methods etc...). Depending on the language of the project, they will have a `.cs` or `.vb` extension.
- **Component class** scaffolds a class that provides a skeleton implementation of a component. Components are used for automation in tandem with data contexts, and are mostly used in workbook apps.
- **Excel function class**: scaffolds a class file that provides an example implementation of a custom Excel function.
- **Settings class**: scaffolds a settings class that will allow the user to configure the app. There is a maximum of one settings class per app.
- **Ribbon class** scaffolds a class that defines a ribbon tab for the app. Ribbon controls provide an easy way for users to communicate with the app. There is a maximum of one ribbon class per app.
- **Context command class** scaffolds a class that defines a context menu command that should be available in Excel. Any number of context menu commands can be defined in an app.
- **Existing file (as content)** imports an existing file from the file system into the project's `content` folder. This folder's contents are embedded in the output dll as embedded resources. Primarily used for images, text files and other resources.
- **Local dll reference** adds a reference to a dll from the local file system to the project. On build, the dll file is copied into the bin folder.
- **Data context files** are script files that define the tables, variables, and events that will be visible to scripts and components.
- **Script files** contain SQL and C# scripts (VB.NET is not supported for scripting) that can process Excel data, fetch data from the outside world or perform other arbitrary tasks. SQL scripts support a preprocessor syntax that allows defining functions and commands via SQL.

### The `project.config` file

Aside from the files in the above menu, the project defines one additional file called  `project.config`. This file contains the configuration settings for the project, including a list of library and NuGet references, language selection (CSharp/VisualBasic), connection strings, and project metadata (name, version, etc...). To read more about the `project.config` file, click [here](project_config_file.md).

## Project folders

Project files can be organized into more-or-less arbitrary folders. The exceptions are the three folders that have a special meaning, specifically: `bin`, `lib` and `content`.

### The `bin` folder

In order for the code inside of a project to be executed, the project must be built (compiled). The output of the build process is stored inside the `bin` folder and represents an app that can be loaded by the runtime.

![Bin folder](../images/projects_bin_folder.png)

The contents of the bin folder are cleared before each build, so it should not be used to store any user files.

After each build, the `bin` folder will contain all of the files that the app consists of:

- dlls from referenced NuGet packages
- locally referenced dlls
- dlls from the `lib` folder
- referenced QueryStorm library dlls
- referenced native dlls
- build output files (dll, xml, pdb)
- a manifest file

Native dlls will go into the `x86` and `x64` subfolders. This is done automatically for dlls listed in the `NativeReferences` section in the `project.config` file.

### The `lib` folder

The `lib` folder contains dll files that were generated ad-hoc by the project, specifically: the data context dlls and a dll with strongly typed wrapper classes for the data context tables.

All other dlls (NuGet dlls, local file system dlls) are referenced from their location on the file system, and are not copied to the `lib` folder. Dlls in the `lib` folder are automatically referenced by the project and copied into the `bin` folder on each build.

### The `content` folder

The `content` folder is a special folder for storing resource files. These are typically images, text or data files used by the app.

Files can be added to the `content` folder from the context menu of the project node.

![Add image as content](../images/add_content_file.png)

Files in the `content` folder are automatically embedded into the output dll as embedded resources.

Helper methods for reading these files are defined in the `QueryStorm.Apps.ResourcesHelper` class:

![Add image as content](../images/add_content_file_explorer.png)

```csharp
// read file as image
System.Drawing.Image image = ResourcesHelper.ReadAsImage("bulb.png");

// read file as text
string text = ResourcesHelper.ReadAsString("frequency_dictionary_en_82_765.txt");
```
