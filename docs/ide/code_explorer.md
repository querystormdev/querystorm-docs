# Code explorer

## Find files and folders

To look for files and folders, use the filter box in the code explorer. Multiple search terms can be entered, separated by spaces. In order for a node to satisfy the filter, all search terms need to match the node or at least one of its ancestors.

![Find file](../images/ide_find_file.gif)

> You can narrow down the search by entering one of the node's ancestors as a search term.
> If a folder satisfies the search criteria, all of its descendant nodes will also be visible.

## Find files with text

To find a file that contains specific text put the text in quotes.

![Find file](../images/ide_find_file_with_text.gif)

> Text search can be combined with file/folder search, so you can, for example, look for files that contain the specified text but only inside a specific folder like so: `theFolder "text to search for"`

## Rename file

To rename a file, select it and press ++f2++. Press ++enter++ to accept the new name or ++esc++ to cancel.

![Rename file](../images/ide_rename_file.gif)

> Files get their extension when they are created, subsequent changes to their extensions are not allowed.

## Move files and folders

Use drag and drop to move files and folders.

![Move files](../images/ide_move_files.gif)

> Some files and folders will refuse to be moved (e.g. bin and lib)
