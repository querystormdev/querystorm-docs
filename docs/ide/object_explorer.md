# Object explorer

The object explorer displays variables and objects that are visible to the current script. It is only visible when a script is the active document in the workspace.

## Go to workbook object

Go to a workbook object by holding ++ctrl++ and clicking the node in the object explorer.

![Go to workbook object](../images/ide_goto_wb_object.gif)

## Rename workbook object

Press ++f2++ to rename a workbook table, column, or named range.

![Rename workbook object](../images/ide_rename_wb_object.gif)

> Renaming database objects is currently not supported.

## Drop to editor

Drag and drop a node into the editor to insert its name. Hold down ++alt++ while dropping to insert all of the node's children's names instead.

![Drop node in editor](../images/ide_drop_node_in_editor.gif)

> Names are quoted if needed.
