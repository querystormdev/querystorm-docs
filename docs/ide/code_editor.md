# Code editor

The QueryStorm IDE offers a rich code editing experience with support for syntax highlighting, intellisense, code fixers, snippets and other useful features.

## Font size

Hold down ++ctrl++ while scrolling the mouse wheel to zoom in or out.

![Scroll to zoom](../images/ide_scroll_to_zoom.gif)

## Code completion

Code completion is automatic in QueryStorm, but you can also invoke it explicitly by pressing ++ctrl+space++.

![Code completion](../images/ide_code_completion.gif)

## Code formatting

Messy code can be auto-formatted by pressing ++ctrl+shift+enter++.

![Code formatting](../images/ide_code_formatting.gif)

## Go to matching bracket

Press ++ctrl+bracket-right++ to navigate to the matching bracket. Hold down the ++shift++ key to also select everything in between.

![Go to matching bracket](../images/ide_bracket_goto.gif)

## Go to symbol definition

Move the caret on top of a symbol, and press ++f12++ to navigate to the symbol definition. This can be in a different file.

![Go to symbol](../images/ide_goto_symbol.gif)

## Rename symbol

Move the caret on top of a symbol and press ++f2++ to rename it. This works across all files in the same project.

![Rename symbol](../images/ide_rename_symbol.gif)

## Code fixers

The IDE offers code fixers that are available at the position of the cursor. To open a list of code fixers click the bulb icon in the left margin of the editor or press the ++ctrl+period++ shortcut key.

![IDE code fixers](../images/ide_code_fixer.gif)

## Add namespace

Adding a namespace for a class can be done using a code fixer, similarly as in Visual Studio.

![IDE add namespace](../images/ide_add_namespace.gif)

## Implement an interface

The easiest way to implement an interface is using the "Implement interface" code fixer.

![IDE implement interface](../images/ide_implement_interface.gif)

## Generate event handler method

An easy way to generate an event handler method is to write the name of a non-existing method as the handler, and then use the "Generate method" code fixer to scaffold it.

![IDE generate event handler](../images/ide_generate_event_handler.gif)

## Override a method

Member overrides can be generated using code-completion. Enter the keyword `override` followed by a space and auto-complete will offer a list of overridable members. Activating an option will scaffold the code for the overriden member.

![IDE override method](../images/ide_override_method.gif)

## Star expansion (SQL)

Move the caret on top of a star symbol in the select list and invoke code-completion (++ctrl+space++) to expand the star into the columns it refers to.

![Star expansion](../images/ide_star_expansion.gif)

> Column names are quoted if necessary.

## Column disambiguation (SQL)

Move the caret on top of an ambiguous symbol and invoke code-completion (++ctrl+space++) to disambiguate it.

![Column disambiguation](../images/ide_column_disambiguation.gif)

> Column names are quoted if necessary.

## Find in file

Press ++ctrl+f++ to find a string inside the current file.

![Find in file](../images/ide_find_text.gif)

> Supports regular expressions.

## Replace in file

Press ++ctrl+h++ to replace occurrences of string inside the current file.

![Replace in file](../images/ide_replace_text.gif)

> Supports regular expressions and replacement patterns.

## Move lines up or down

To move a line up or down, hold down the ++alt++ key while pressing ++up++ or ++down++ arrow keys.

![Move lines](../images/ide_move_lines.gif)

## Code snippets

Use code snippets to write code faster. Use the ++tab++ key to navigate between snippet placeholders, press ++enter++ to complete the snippet.

![Snippets](../images/ide_snippets.gif)

> Snippets are language-specific, so depending on the current file, different snippets will be offered.

## Configure shortcut keys

Shortcut keys can be configured in the plugin settings

![Shortcuts](../images/ide_shortcuts.png)
