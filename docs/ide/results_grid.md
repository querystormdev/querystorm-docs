# Results grid

## Sorting results

Click a column header to sort based on the column. Hold down ++ctrl++ and click a different column to include the column as a secondary (or tertiary, or...) sort criteria.

![Sorting results](../images/ide_sort_results.gif)

## Go to address in Excel

If a row in the results grid contains an address, double-clicking the address, or the row header will select the row in Excel. Select multiple rows and press space to select multiple rows in Excel.

![Select workbook address](../images/ide_select_wb_address.gif)
