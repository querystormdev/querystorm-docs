# IDE Layouts

The IDE supports three different preset positions:

- Bottom
- Right
- Undocked

Each of the three layouts is independent of the others, so moving panes in one layout does not affect pane positions in the other two. Switching between layouts as well as resetting a layout is done using ribbon buttons.

![IDE layouts](../images/ide_layout.gif?v=2)
