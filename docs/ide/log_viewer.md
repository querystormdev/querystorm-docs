# Messages log

The message log shows compile-time as well as run-time messages. Messages can be filtered by text or severity.

## Message tooltips

Hover over an item to see a tooltip with its full contents. This is particularly helpful for errors since they tend to include multiple lines of text.

![Hover log item](../images/ide_hover_log_item.gif)

## Navigate to an error in the source code

Double-click an error to navigate to its location in the source code.

![Go to error line](../images/ide_go_to_error_line.gif)

> This does not work for errors that do not have source line information.
